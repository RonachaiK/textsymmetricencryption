﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextSymmetric
{
    public abstract class Cryptography
    {
        private string _painText;
        private readonly string _passPhrase;
        private readonly string _hashType;
        private readonly int _keySize = 128;
        private const string _saltValue = "SaltTextValue";
        private const string _innitVector = "1234567890111213";// must be 16 bytes
        private readonly int _passIteration = 2;

        public string PainText
        {
            get { return _painText; }
            set { _painText = value; }
        }

        protected byte[] PainTextBytes
        {
            get { return Encoding.UTF8.GetBytes(_painText); }

        }
        protected byte[] PassBytes
        {
            get { return Encoding.UTF8.GetBytes(_passPhrase); }

        }
        protected byte[] SaltBytes
        {
            get { return Encoding.UTF8.GetBytes(_saltValue); }

        }
        protected byte[] VectorBytes
        {
            get { return Encoding.UTF8.GetBytes(_innitVector); }

        }
        protected string HashType
        {
            get
            {
                return _hashType;
            }
        }
        protected int PasswordIteration
        {
            get { return _passIteration; }
        }
        protected int Size
        {
            get { return _keySize; }
        }
        public Cryptography(string text, string passPharase, HashType.Type hashType, EncryptKeySize.Size keySize, int passwordInteration)
        {
            _painText = text;
            _passPhrase = passPharase;
            _hashType = hashType.GetTypeString();
            _keySize = (int)keySize;
            _passIteration = passwordInteration;
        }
     
    }

}
