﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
namespace TextSymmetric
{
    public sealed class TextEncoder : Cryptography, IEncoder
    {
        public TextEncoder(string text, string passPharase, HashType.Type hashType, EncryptKeySize.Size keySize, int passwordInteration) : base(text, passPharase, hashType, keySize, passwordInteration)
        {
            if (text == "")
                throw new ArgumentException("Text much not be null or empty.");
            if (passPharase == "")
                throw new ArgumentException("PassPharase much not be null or empty.");
            if (passwordInteration == 0)
                throw new ArgumentException("PasswordInteration much more than 0.");
        }

        public string Encrypt()
        {
            PasswordDeriveBytes password = new PasswordDeriveBytes
                (
                    PassBytes,
                    SaltBytes,
                    HashType,
                    PasswordIteration
                );

            byte[] keyBytes = password.GetBytes(Size / 8);

            // Create uninitialized Rijndael encryption object.
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;

            // Generate encryptor from the existing key bytes and initialization 
            // vector. Key size will be defined based on the number of the key bytes.
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor
            (
                keyBytes,
                VectorBytes
            );

            // Define memory stream which will be used to hold encrypted data.
            MemoryStream memoryStream = new MemoryStream();

            // Define cryptographic stream (always use Write mode for encryption).
            CryptoStream cryptoStream = new CryptoStream
            (
                memoryStream,
                encryptor,
                CryptoStreamMode.Write
            );

            // Start encrypting.
            cryptoStream.Write(PainTextBytes, 0, PainTextBytes.Length);

            // Finish encrypting.
            cryptoStream.FlushFinalBlock();

            // Convert our encrypted data from a memory stream into a byte array.
            byte[] cipherTextBytes = memoryStream.ToArray();

            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();

            // Convert encrypted data into a base64-encoded string.
            string cipherText = Convert.ToBase64String(cipherTextBytes);

            // Return encrypted string.
            return cipherText;


        }
     
    }
}
