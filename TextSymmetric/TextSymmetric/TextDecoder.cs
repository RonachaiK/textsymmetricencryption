﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
namespace TextSymmetric
{
    public sealed class TextDecoder : Cryptography, IDecoder
    {

        public TextDecoder(string cipherText, string passPharase, HashType.Type hashType, EncryptKeySize.Size keySize, int passwordInteration) : base(cipherText, passPharase, hashType, keySize, passwordInteration)
        {
            if (cipherText == "")
                throw new ArgumentException("CipherText much not be null or empty.");
            if (passPharase == "")
                throw new ArgumentException("PassPharase much not be null or empty.");
            if (passwordInteration == 0)
                throw new ArgumentException("PasswordInteration much more than 0.");
        }

        public string Decrypt()
        {
            string plainText = "";
            try
            {
                PasswordDeriveBytes password = new PasswordDeriveBytes
                (
                 PassBytes,
                 SaltBytes,
                 HashType,
               PasswordIteration
               );

                byte[] keyBytes = password.GetBytes(Size / 8);
                byte[] cipherTextBytes = Convert.FromBase64String(PainText);
                // Create uninitialized Rijndael encryption object.
                RijndaelManaged symmetricKey = new RijndaelManaged();
                symmetricKey.Mode = CipherMode.CBC;

                ICryptoTransform decryptor = symmetricKey.CreateDecryptor
                (
                    keyBytes,
                    VectorBytes
                );
                MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
                CryptoStream cryptoStream = new CryptoStream
                (
                    memoryStream,
                    decryptor,
                    CryptoStreamMode.Read
                );
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                // Start decrypting.
                int decryptedByteCount = cryptoStream.Read
                (
                    plainTextBytes,
                    0,
                    plainTextBytes.Length
                );

                // Close both streams.
                memoryStream.Close();
                cryptoStream.Close();

                // Convert decrypted data into a string. 
                // Let us assume that the original plaintext string was UTF8-encoded.
                plainText = Encoding.UTF8.GetString
               (
                   plainTextBytes,
                   0,
                   decryptedByteCount
               );


            }
            catch
            {
                throw new Exception("Cannot decrypt cipher text.");
            }
            return plainText;
        }
    }
}
