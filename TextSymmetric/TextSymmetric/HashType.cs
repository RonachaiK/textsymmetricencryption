﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextSymmetric
{
    public static class HashType
    {
        public enum Type { MD5, SHA1, SHA256, SHA384, SHA512 }
        public static string GetTypeString(this Type type) {
            switch (type)
            {
                case Type.SHA1: return "SHA1";
                case Type.SHA256: return "SHA256";
                case Type.SHA384: return "SHA384";
                case Type.SHA512: return "SHA512";
                case Type.MD5: return "MD5";
                default: return "SHA256";
            }
        }
    }
}
