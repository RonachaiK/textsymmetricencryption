﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextSymmetric
{
    public static class EncryptKeySize
    {
        public enum Size { Size128 = 128, Size192 = 192, Size256 = 256 }
    }
}
