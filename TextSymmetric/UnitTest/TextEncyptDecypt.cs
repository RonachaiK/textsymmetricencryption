﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TextSymmetric;
namespace UnitTest
{
    [TestClass]
    public class TextEncyptDecypt
    {
        [TestMethod]
        public void Encypt_Should_Be_Equal_Decypted_With_All_Input()
        {
            string plainText = "Welcome";
            string passPhrase = "Passphrase";
            int passwordIterations = 2;
            IEncoder encoder = new TextEncoder(plainText, passPhrase, HashType.Type.SHA256, EncryptKeySize.Size.Size256, passwordIterations);
            string cipherText = encoder.Encrypt();
            IDecoder decoder = new TextDecoder(cipherText, passPhrase, HashType.Type.SHA256, EncryptKeySize.Size.Size256, passwordIterations);
            Assert.AreEqual(plainText, decoder.Decrypt());
        }

        [TestMethod]
        public void Invalid_Encrypt_With_Empty_PlainText()
        {
            string plainText = "";
            string passPhrase = "Passphrase";
            int passwordIterations = 2;
            string msg = "";
            try
            {
                IEncoder encoder = new TextEncoder(plainText, passPhrase, HashType.Type.SHA256, EncryptKeySize.Size.Size256, passwordIterations);
                encoder.Encrypt();

            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            Assert.AreEqual("Text much not be null or empty.", msg);


        }
        [TestMethod]
        public void Invalid_Encrypt__With_Empty_Passphrase()
        {
            string plainText = "Welcome";
            string passPhrase = "";
            int passwordIterations = 2;
            string msg = "";
            try
            {
                IEncoder encoder = new TextEncoder(plainText, passPhrase, HashType.Type.SHA256, EncryptKeySize.Size.Size256, passwordIterations);
                encoder.Encrypt();

            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            Assert.AreEqual("PassPharase much not be null or empty.", msg);


        }
        [TestMethod]
        public void Invalid_Encrypt__With_PasswordIterationse_Equal_0()
        {
            string plainText = "Welcome";
            string passPhrase = "Passphrase";
            int passwordIterations = 0;
            string msg = "";
            try
            {
                IEncoder encoder = new TextEncoder(plainText, passPhrase, HashType.Type.SHA256, EncryptKeySize.Size.Size256, passwordIterations);
                encoder.Encrypt();

            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            Assert.AreEqual("PasswordInteration much more than 0.", msg);


        }
        [TestMethod]
        public void Invalid_Decrypt_With_Empty_CipherText()
        {

            string msg = "";
            try
            {

                IDecoder decoder = new TextDecoder("", "Passphrase", HashType.Type.SHA256, EncryptKeySize.Size.Size256, 2);
                decoder.Decrypt();
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            Assert.AreEqual("CipherText much not be null or empty.", msg);


        }
        [TestMethod]
        public void Invalid_Decrypt_With_Empty_Passphrase()
        {

            string passPhrase = "";
            string msg = "";
            try
            {

                IDecoder decoder = new TextDecoder("asdfasdfasdf", passPhrase, HashType.Type.SHA256, EncryptKeySize.Size.Size256, 2);
                decoder.Decrypt();
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            Assert.AreEqual("PassPharase much not be null or empty.", msg);


        }

        [TestMethod]
        public void Invalid_Decrypt_With_Empty_PasswordIterationse_Equal_0()
        {

            string msg = "";
            try
            {

                IDecoder decoder = new TextDecoder("asdfasdfasdf", "asdfasdfasdf", HashType.Type.SHA256, EncryptKeySize.Size.Size256, 0);
                decoder.Decrypt();
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            Assert.AreEqual("PasswordInteration much more than 0.", msg);


        }
        [TestMethod]
        public void Cannot_Decrypt_CipherText()
        {

            string msg = "";
            try
            {

                IDecoder decoder = new TextDecoder("asdfasdfasdf", "asdfasdfasdf", HashType.Type.SHA256, EncryptKeySize.Size.Size256, 2);
                decoder.Decrypt();
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            Assert.AreEqual("Cannot decrypt cipher text.", msg);


        }

    }
}
