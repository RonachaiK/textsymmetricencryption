Symmetric-key algorithms are algorithms for cryptography that use the same cryptographic keys for both encryption of plaintext and decryption of ciphertext. The keys may be identical or there may be a simple transformation to go between the two keys.

Reference : http://www.csharpstar.com/csharp-encrypt-and-decrypt-data-using-symmetric-key/